---
title: Marke
description: Marke
ranking: 0
featuredgrid: 2
featured:
  - url: marke-01.jpg
    credit: Pamela Cecchi
  - url: marke-04.jpg
    credit: Pamela Cecchi
gallerygrid: 4
gallery:
  - url: marke-03.jpg
    credit: Pamela Cecchi
  - url: marke-05.jpg
    credit: Pamela Cecchi
  - url: marke-06.jpg
    credit: Pamela Cecchi
  - url: marke-07.jpg
    credit: Pamela Cecchi
  - url: marke-08.jpg
    credit: Pamela Cecchi
  - url: marke-09.jpg
    credit: Pamela Cecchi
  - url: marke-10.jpg
    credit: Pamela Cecchi
  - url: marke-11.jpg
    credit: Pamela Cecchi
  - url: marke-12.jpg
    credit: Pamela Cecchi
  - url: marke-13.jpg
    credit: Pamela Cecchi
  - url: marke-14.jpg
    credit: Pamela Cecchi
  - url: marke-15.jpg
    credit: Pamela Cecchi

---

## Sortir du cadre…

Sortir la toile de son cadre, la recontextualiser, voir comment elle se porte, se comporte, se transporte. Depuis longtemps, je suis fascinée par ce travail de patience, ces pixels de fils que sont les tableaux en points de croix, les canevas. Ces hommes et ces femmes qui tissent, tressent, nouent ces cotons de couleurs chaleureuses pour évoquer scènes de vie, de chasse, portraits ou autres reproductions de toiles célèbres.
Je veux rendre hommage à ce travail en habillant des corps en mouvement de ces images d’un autre temps. Voir porter aujourd’hui sur des hanches le temps du corset, de l’eau du puits et de la diligence.

<p class="sign">Marie Kersten</p>

<Gallery :gallery="featured" :gallerygrid="featuredgrid"></Gallery>
<Gallery :gallery="gallery" :gallerygrid="gallerygrid"></Gallery>