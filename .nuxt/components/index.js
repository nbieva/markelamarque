import { wrapFunctional } from './utils'

export { default as Contact } from '../../components/Contact.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as Gallery } from '../../components/Gallery.vue'

export const LazyContact = import('../../components/Contact.vue' /* webpackChunkName: "components/contact" */).then(c => wrapFunctional(c.default || c))
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/footer" */).then(c => wrapFunctional(c.default || c))
export const LazyGallery = import('../../components/Gallery.vue' /* webpackChunkName: "components/gallery" */).then(c => wrapFunctional(c.default || c))
