export default {
  ssr: false,
  target: 'static',
  head: {
    title: 'MARKE | Design de mode. Pièces uniques.',
    htmlAttrs: {
      lang: 'fr'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'theme-color', content: '#000000' },
      { name: 'msapplication-TileColor', content: '#000000' },
      { name: 'msapplication-navbutton-color', content: '#000000' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#000000' }, 
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { property: 'og:title', content: 'MARKE' },
      { property: 'og:description', content: 'MARKE | Design de mode. Pièces uniques.' },
      { property: 'og:image:type', content: 'image/png' },
      { property: 'og:image', content: 'social-pic.png' },
      { hid: 'description', name: 'description', content: 'MARKE | Design de mode. Pièces uniques.' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Birthstone+Bounce:wght@400;500&display=swap' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,500;0,600;0,700;0,800;0,900;1,400;1,500;1,600;1,700;1,800;1,900&display=swap' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Bitter:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap' }
    ],
    script: [
      { src: '//code.jquery.com/jquery-3.3.1.min.js' },
      { src: 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.js' }
    ]
  },
  loading: { color: '#3d575a' },
  css: [
    '@/assets/scss/styles.scss'
  ],
/*   plugins: [
    { src: "@/plugins/aos", mode: "client" }
  ], */
  components: true,
  buildModules: [],
  modules: [
    'nuxt-material-design-icons',
    '@nuxtjs/style-resources',
    '@nuxtjs/axios',
    '@nuxt/content',
    '@nuxtjs/color-mode'
  ],
  styleResources: {
    scss: [
        '@/assets/scss/styles.scss',
        '@/assets/scss/variables.scss'
    ]
  },
  axios: {},
  content: {},
  generate: {}
}